import React, { useState } from 'react';
import { getAuth } from './utils/GerarCookies';
import './App.css';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom'
import 'bootstrap/dist/js/bootstrap.js';

import BemVindo from './components/sobre/BemVindo'
import 'react-datepicker/dist/react-datepicker.css';
import { CSSTransition } from 'react-transition-group';

import { setFechaPesquisaTrocaPage, getFechaPesquisaTrocaPage ,setActivePage, getActivePage, setActivePesquisaAgenda, setActivePesquisaPage, setLiberaFetch, getFecharTudo, setFecharTudo } from './utils/Globals';

import './App.css'
import './css/bootstrap.min.css';
import './css/global.css';

import Login from './img/icon/login.ico';
import Cadastro from './img/icon/adduser.ico'
import Agenda from './img/icon/agenda-home.png';
import Pagina from './img/icon/criar-pagina.ico';
import Pesquisa from './img/icon/pesquisa.ico';
import Sair from './img/icon/exit.ico';
import Nova from './img/icon/criar.ico';
import User from './img/icon/edit-user.ico';
import Seta from './img/icon/seta-menu.ico';
import Sobre from './img/icon/sobre.ico';


const zeraProps = (setAddColorEditUser, setAddColorPesquisa, setAddColorCad, setAddColorLogin, setAddColorSair, setAddColorNovaAgenda, setAddColorNovaPagina, setAddColorSobre) => {
  setAddColorCad(false);
  setAddColorLogin(false);
  setAddColorSair(false);
  setAddColorNovaAgenda(false);
  setAddColorNovaPagina(false);
  setAddColorPesquisa(false);
  setAddColorEditUser(false);
  setActivePesquisaPage(false);
  setActivePesquisaAgenda(false);
  setFecharTudo(false);
  setAddColorSobre(false);
}


const logado = (auth, addColorSair, setAddColorSair, addColorNovaAgenda, setAddColorNovaAgenda, 
  setAddColorLogin, setAddColorCad, addColorNovaPagina, setAddColorNovaPagina, addColorPesquisa, 
  setAddColorPesquisa, addColorEditUser, setAddColorEditUser, setAtual, atual, addColorSobre, setAddColorSobre) => {
  
  if(getActivePage()){
    if(getFechaPesquisaTrocaPage()){
      setFecharTudo(true);
      setFechaPesquisaTrocaPage(false);
    }
  }else{
    setFechaPesquisaTrocaPage(true);
  }

  if(getFecharTudo()){
    zeraProps(setAddColorEditUser, setAddColorPesquisa, setAddColorCad, setAddColorLogin, setAddColorSair, setAddColorNovaAgenda, setAddColorNovaPagina, setAddColorSobre);
  }

  if(window.location.pathname === '/agendas'){
    setActivePage(false);
    atual !== '/agendas' && setAtual('/agendas');
    
    
  }
  if(window.location.pathname === '/agenda/paginas'){
    setActivePage(true);
    atual !== '/agendas' && setAtual('/agendas');
  }

  if(window.location.pathname === '/agenda/pagina/editar'){
    atual !== '/agenda/paginas' && setAtual('/agenda/paginas');
  }
  
  
  return (
    <ul className='ul-nav-edit'>
      <Link
      to={getActivePage() ? '/agenda/paginas' : '/agendas'}
      onClick={() => {
        setAddColorLogin(false);
        setAddColorCad(false);
        setAddColorSair(false);
        setAddColorNovaAgenda(false);
        setAddColorNovaPagina(false);
        setAddColorPesquisa(true);
        setAddColorEditUser(false);
        setAddColorSobre(false);

        if(getActivePage()){
          setActivePesquisaPage(true);
          setActivePesquisaAgenda(false);
        }else{
          setActivePesquisaPage(false);
          setActivePesquisaAgenda(true);
        }
      }}
      className='link-li-edit'
      >
        <li className={addColorPesquisa ? 'li-nav-edit color-edit' : 'li-nav-edit'} >
          <img className='img-link-edit' src={Pesquisa} alt='cadastro' />
        </li>
      </Link>

      {getActivePage()
        ? <Link
            onClick={() => {
              setAtual('/agenda/paginas');
              setAddColorNovaAgenda(false);
              setAddColorNovaPagina(true);
              setAddColorPesquisa(false);
              setAddColorSair(false);
              setAddColorEditUser(false);
              setActivePesquisaPage(false);
              setActivePesquisaAgenda(false);
              setAddColorSobre(false);
            }}
            className='link-li-edit'
            to='/agenda/nova-pagina'>
            <li className={addColorNovaPagina ? 'li-nav-edit color-edit' : 'li-nav-edit'} >
              <img className='img-link-edit' src={Pagina} alt='login' />
            </li>
          </Link>
        : <Link
            onClick={() => {
              setAddColorNovaAgenda(true);
              setAddColorNovaPagina(false);
              setAddColorPesquisa(false);
              setAddColorSair(false);
              setAddColorEditUser(false);
              setActivePesquisaPage(false);
              setActivePesquisaAgenda(false);
              setAddColorSobre(false);
            }}
            className='link-li-edit'
            to='/nova-agenda'>
            <li className={addColorNovaAgenda ? 'li-nav-edit color-edit' : 'li-nav-edit'} >
              <img className='img-link-edit' src={Nova} alt='login' />
            </li>
          </Link>
      }
      <Link
      onClick={() => {
        setAddColorLogin(false);
        setAddColorCad(false);
        setAddColorSair(false);
        setAddColorNovaAgenda(false);
        setAddColorNovaPagina(false);
        setAddColorPesquisa(false);
        setAddColorEditUser(true);
        setLiberaFetch(true);
        setActivePesquisaPage(false);
        setActivePesquisaAgenda(false);
        setAddColorSobre(false);
      }}
      className='link-li-edit'
      to='/usuario-editar'>
      <li className={addColorEditUser ? 'li-nav-edit color-edit' : 'li-nav-edit'} >
        <img className='img-link-edit' src={User} alt='cadastro' />
      </li>
    </Link>

    <Link
      onClick={() => {
        setAddColorLogin(false);
        setAddColorCad(false);
        setAddColorSair(false);
        setAddColorNovaAgenda(false);
        setAddColorNovaPagina(false);
        setAddColorPesquisa(false);
        setAddColorEditUser(false);
        setActivePesquisaPage(false);
        setActivePesquisaAgenda(false);
        setAddColorSobre(true);
      }}
      className='link-li-edit'
      to='/sobre'>
      <li className={addColorSobre ? 'li-nav-edit color-edit' : 'li-nav-edit'} >
        <img className='img-link-edit' src={Sobre} alt='cadastro' />
      </li>
    </Link>

      <Link
        onClick={() => {
          setAddColorLogin(false);
          setAddColorCad(false);
          setAddColorSair(false);
          setAddColorNovaAgenda(false);
          setAddColorNovaPagina(false);
          setAddColorPesquisa(false);
          setAddColorEditUser(false);
          setActivePesquisaPage(false);
          setActivePesquisaAgenda(false);
          setAddColorSobre(false);
        }}
        className='link-li-edit'
        to='/sair'>
        <li className={addColorSair ? 'li-nav-edit color-edit' : 'li-nav-edit'} >
          <img className='img-link-edit' src={Sair} alt='cadastro' />
        </li>
      </Link>

      <Redirect to='/agendas' />
    </ul>
  )
}

const deslogado = (addColorLogin, setAddColorLogin, addColorCad, setAddColorCad, addColorSobre, setAddColorSobre, setAparecer) => {
  return (
    <ul className='ul-nav-edit'>
      <Link
        onClick={() => {
          setAddColorLogin(true);
          setAddColorCad(false);
          setAddColorSobre(false);
          setAparecer(false);
        }}
        className='link-li-edit'
        to='/entrar'>
        <li className={addColorLogin ? 'li-nav-edit color-edit' : 'li-nav-edit'} >
          <img className='img-link-edit' src={Login} alt='login' />
        </li>
      </Link>
      <Link
        onClick={() => {
          setAddColorLogin(false);
          setAddColorCad(true);
          setAddColorSobre(false);
          setAparecer(false);
        }}
        className='link-li-edit'
        to='/cadastrar'>
        <li className={addColorCad ? 'li-nav-edit color-edit' : 'li-nav-edit'} >
          <img className='img-link-edit' src={Cadastro} alt='cadastro' />
        </li>
      </Link>
      <Link
      onClick={() => {
        setAddColorLogin(false);
        setAddColorCad(false);
        setActivePesquisaPage(false);
        setActivePesquisaAgenda(false);
        setAddColorSobre(true);
        setAparecer(false);
      }}
      className='link-li-edit'
      to='/sobre'>
      <li className={addColorSobre ? 'li-nav-edit color-edit' : 'li-nav-edit'} >
        <img className='img-link-edit' src={Sobre} alt='cadastro' />
      </li>
    </Link>
    </ul>
  )
}



const App = () => {
  const [addColorLogin, setAddColorLogin] = useState(false);
  const [addColorCad, setAddColorCad] = useState(false);
  const [addColorSair, setAddColorSair] = useState(false);
  const [addColorNovaAgenda, setAddColorNovaAgenda] = useState(false);
  const [addColorNovaPagina, setAddColorNovaPagina] = useState(false);
  const [addColorPesquisa, setAddColorPesquisa] = useState(false);
  const [addColorEditUser, setAddColorEditUser] = useState(false);
  const [addColorSobre, setAddColorSobre] = useState(false);
  const [aparecer,setAparecer] = useState(true);
  const [showMenu,setShowMenu] = useState(true);
  const [atual,setAtual] = useState('/agendas');


  const auth = getAuth();


  return (
    <div> 
      <Link
      className='link-img-ag'
      to={auth ? atual : '/'}
      onClick={() => {
        setActivePage(false);
        setAddColorCad(false);
        setAddColorLogin(false);
        setAddColorSair(false);
        setAddColorNovaAgenda(false);
        setAddColorNovaPagina(false);
        setAddColorPesquisa(false);
        setAddColorEditUser(false);
        setActivePesquisaPage(false);
        setActivePesquisaAgenda(false);
        setAddColorSobre(false);
        setAparecer(true);
      }}
    >
      <img className='img-nav-edit-idx' src={Agenda} alt='cadastro' />
    </Link>
    <div 
    className ='div-seta-menu-edit'
    onClick={()=>{
      setShowMenu(!showMenu);
    }}
    >
      <img className='seta-menu-edit' src={Seta} alt='seta do menu'/>
    
    </div>
    <CSSTransition in={showMenu} timeout={300} classNames="my-node">
      <nav className={auth ? 'navbar-edit': 'navbar-edit-deslogado'}>
      <div className='container'>
        {auth 
          ? logado(auth, addColorSair, setAddColorSair, addColorNovaAgenda, 
          setAddColorNovaAgenda, setAddColorLogin, setAddColorCad, addColorNovaPagina, 
          setAddColorNovaPagina, addColorPesquisa, setAddColorPesquisa, 
          addColorEditUser, setAddColorEditUser, setAtual, atual, addColorSobre, setAddColorSobre) 
          : deslogado(addColorLogin, setAddColorLogin, addColorCad, setAddColorCad, addColorSobre, setAddColorSobre, setAparecer)}
      </div>
      </nav>
    </CSSTransition>
            {aparecer && !auth && <BemVindo/>}
    </div>
  );
}

export default App;
