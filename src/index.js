import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Route,BrowserRouter as Router } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

import App from './App';
import CriarConta from './components/login/CriarConta';
import Entrar from './components/login/Entrar';
import Sair from './components/login/Sair';
import CriarAgenda from './components/agenda/CriarAgenda';
import ListarAgendas from './components/agenda/ListarAgendas';
import ExcluirAgenda from './components/agenda/ExcluirAgenda';
import EditarAgenda from './components/agenda/EditarAgenda';
import ListarPaginas from './components/pagina/ListarPaginas';
import ExcluirPagina from './components/pagina/ExcluirPagina';
import EditarPagina from './components/pagina/EditarPagina';
import CriarPagina from './components/pagina/CriarPagina';
import EditarConta from './components/login/EditarConta';
import Sobre from './components/sobre/Sobre'


ReactDOM.render(
    (<Router>
        <div>
            <Route path='/' component={App}/>
            <Route path='/agendas' component={ListarAgendas}/>
            <Route path='/agenda/pagina/excluir' component={ExcluirPagina}/>
            <Route path='/agenda/pagina/editar' component={EditarPagina}/>
            <Route path='/agenda/paginas' component={ListarPaginas}/>
            <Route path='/agenda/nova-pagina' component={CriarPagina}/>
            <Route path='/agenda/excluir' component={ExcluirAgenda}/>
            <Route path='/agenda/editar' component={EditarAgenda}/>
            <Route path='/usuario-editar' component={EditarConta}/>
            <Route path='/entrar' component={Entrar}/>
            <Route path='/cadastrar' component={CriarConta}/>
            <Route path='/nova-agenda' component={CriarAgenda}/>
            <Route path='/sobre' component={Sobre}/>
            <Route path='/sair' component={Sair}/>
        </div>
    </Router>), 
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();