import { getToken, setCookie, getIdUser } from '../../../utils/GerarCookies';
import { getLiberaFetch } from '../../../utils/Globals';
import jso from '../../../server.json'


export const Cadastrar = (nome, sobrenome, cpf, email, senha, setTipoSucesso,
    setTipoCpfError, setTipoNomeError, setTipoSobrenomeError,
    setTipoEmailError, setTipoSenhaError, setMsgEmail, setMsgCpf,
    setMsgSenha, setMsgSobrenome, setMsgNome) => {

    const headers = {
        'Content-Type': 'application/json'
    }


    const param = {
        'method': 'POST',
        'body': JSON.stringify({
            "email": email,
            "senha": senha,
            "nivel": 1,
            "info_user": {
                "nome": nome,
                "sobrenome": sobrenome,
                "cpf": cpf
            }
        }),
        'headers': headers
    }

    fetch(`${jso.host}/usuarios`, param)
        .then(response => response.json())
        .then(json => {
            if (!json.erros) {
                if (!json.error) {
                    setTipoSucesso(true);
                }
                else {
                    if (json.error.indexOf("email_pessoa_UNIQUE") >= 0) {
                        setTipoEmailError(true);
                        setMsgEmail('Email já cadastrado')
                    }
                    else if (json.error.indexOf("cpf_pessoa_UNIQUE") >= 0) {
                        setTipoCpfError(true);
                        setMsgCpf('Cpf já cadastrado')
                    }
                    setTipoSucesso(false);
                }
            } else {
                json.erros.forEach(erro => {
                    
                    switch(erro) {
                        case 'erro nome invalido':
                            setTipoNomeError(true);
                            setMsgNome('Nome não é valido');
                            break;
                        case 'erro cpf invalido':
                            setTipoCpfError(true);
                            setMsgCpf('Cpf não é valido');
                            break;
                        case 'erro sobrenome invalido':
                            setTipoSobrenomeError(true);
                            setMsgSobrenome('Sobrenome não é valido');
                            break;
                        case 'erro email invalido':
                            setTipoEmailError(true);
                            setMsgEmail('Email não é valido');
                            break;
                        case 'erro senha invalida':
                            setTipoSenhaError(true);
                            setMsgSenha('Senha não é valida');
                            break;
                        default:
                            break;
                    }
                })
            }
        })
        .catch(e => console.log('erro ao realizar cadastro de usuario:', e));
}

export const Editar = (nome, sobrenome, cpf, email, senha, setTipoSucesso,
    setTipoCpfError, setTipoNomeError, setTipoSobrenomeError,
    setTipoEmailError, setTipoSenhaError, setMsgEmail, setMsgCpf,
    setMsgSenha, setMsgSobrenome, setMsgNome) => {
    
    if(getLiberaFetch){
        const headers = {
            'Content-Type': 'application/json',
            'x-access-token': getToken()
        }
    
    
        const param = {
            'method': 'PUT',
            'body': JSON.stringify({
                "id": getIdUser(),
                "email":email,
                "ativo": 1,
                "senha": senha,
                "nivel":1,
                "info_user":{
                    "nome": nome,
                    "sobrenome": sobrenome,
                    "cpf": cpf
                }
            }),
            'headers': headers
        }
    
        fetch(`${jso.host}/usuarios`, param)
            .then(response => response.json())
            .then(json => {
                if (!json.erros) {
                    if (!json.error) {
                        setTipoSucesso(true);
                    }
                    else {
                        if (json.error.indexOf("email_pessoa_UNIQUE") >= 0) {
                            setTipoEmailError(true);
                            setMsgEmail('Email já cadastrado')
                        }
                        else if (json.error.indexOf("cpf_pessoa_UNIQUE") >= 0) {
                            setTipoCpfError(true);
                            setMsgCpf('Cpf já cadastrado')
                        }
                        setTipoSucesso(false);
                    }
                } else {
                    json.erros.forEach(erro => {
                        
                        switch(erro) {
                            case 'erro nome invalido':
                                setTipoNomeError(true);
                                setMsgNome('Nome não é valido');
                                break;
                            case 'erro cpf invalido':
                                setTipoCpfError(true);
                                setMsgCpf('Cpf não é valido');
                                break;
                            case 'erro sobrenome invalido':
                                setTipoSobrenomeError(true);
                                setMsgSobrenome('Sobrenome não é valido');
                                break;
                            case 'erro email invalido':
                                setTipoEmailError(true);
                                setMsgEmail('Email não é valido');
                                break;
                            case 'erro senha invalida':
                                setTipoSenhaError(true);
                                setMsgSenha('Senha não é valida');
                                break;
                            default:
                                break;
                        }
                    })
                }
            })
            .catch(e => console.log('erro ao realizar cadastro de usuario:', e));
    }
   
}

export const Autenticar = (login, senha, setToken, setAuth, setTipoEmailError, setTipoSenhaError, setMsgEmail, setMsgSenha) => {

    const headers = {
        'Content-Type': 'application/json'
    }

    const param = {
        'method': 'POST',
        'body': JSON.stringify({
            'login': login,
            'senha': senha
        }),
        'headers': headers
    }

    fetch(`${jso.host}/login`, param)
        .then(response => response.json())
        .then(json => {
            if (json.error) {
                if (json.error === 'Email invalido') {
                    setMsgEmail('Email não cadastrado');
                    setTipoEmailError(true);
                    setTipoSenhaError(false);
                } else {
                    setMsgSenha("Senha inválida")
                    setTipoEmailError(false);
                    setTipoSenhaError(true);
                }
            } else {
                setCookie('sessionId', json.sessionId, 0.5);
                setCookie('autenticado', json.auth, 0.5);
                setCookie('tokenSession', json.token, 0.5);

                setAuth(json.auth);
                setToken(json.token);
            }
        })
        .catch(e => console.log('erro ao realizar login:', e));

}

export const LogOff = () => {
    setCookie('autenticado', false);

    const headers = {
        'x-access-token': getToken(),
        'Content-Type': 'application/json'
    }

    const param = {
        'method': 'GET',
        'headers': headers
    }

    fetch(`${jso.host}/logout`, param)
        .then(response => response.json())
        .then(json => {
            setCookie('sessionId', null);
            setCookie('autenticado', json.auth);
            setCookie('tokenSession', json.token);
        })
        .catch(e => console.log('erro ao realizar logoff:', e));
}

export const BuscarUserPorId = (setJson) => {

    const headers = {
        'x-access-token': getToken(),
        'Content-Type': 'application/json'
    }

    const param = {
        'method': 'GET',
        'headers': headers
    }

    fetch(`${jso.host}/usuarios/${getIdUser()}`, param)
        .then(response => response.json())
        .then(json => {
            if(!json.error){
                let nomeAndSobrenome= json[0].info_user.nome.split(' ');
                json[0].info_user.nome = nomeAndSobrenome[0];
                json[0].info_user['sobrenome'] = nomeAndSobrenome[1];
                setJson(json);
            }else{
                console.log(json);
            }
        })
        .catch(e => console.log('erro ao realizar busca por id: ', e));
}