import React, { useEffect } from 'react';
import { LogOff } from './fetch/Login';
import { Redirect } from 'react-router-dom'

const Sair = () =>{

    useEffect(() => {
        
        LogOff();
      
    },[]);
    
    return(
        <div>
            {<Redirect to='/' onUpdate={()=> window.scrollTo(0,0)} /> }
        </div>
        
    )
}

export default Sair;