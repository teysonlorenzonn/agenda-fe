import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom'

import './css/login.css';
import { setFecharTudo } from '../../utils/Globals';
import { Editar, BuscarUserPorId} from './fetch/Login';
import { hasValidEmail, hasValidString, hasValidCpf } from '../../utils/Validacao';
import { cpfMask, anulaEspacoMask } from '../../utils/Mascaras';
import avatarUser from '../../img/icon/edit-users.ico';

const EditarConta = () => {
  const [json, setJson] = useState([]);

  const [valueCpf, setValueCpf] = useState('');
  const [valueNome, setValueNome] = useState('');
  const [valueSobrenome, setValueSobrenome] = useState('');
  const [valueEmail, setValueEmail] = useState('');
  const [valueSenha, setValueSenha] = useState('');
  const [valueReSenha, setValueReSenha] = useState('');


  const [nome, setNome] = useState('');
  const [sobrenome, setSobrenome] = useState('');
  const [cpf, setCpf] = useState('');
  const [email, setEmail] = useState('');
  const [senha, setSenha] = useState('');
  const [reCadSenha, setReCadSenha] = useState('');

  const [msgEmail, setMsgEmail] = useState('');
  const [msgCpf, setMsgCpf] = useState('');
  const [msgNome, setMsgNome] = useState('');
  const [msgSobrenome, setMsgSobrenome] = useState('');
  const [msgSenha, setMsgSenha] = useState('');
  const [msgReSenha, setMsgReSenha] = useState('');

  const [tipoEmailError, setTipoEmailError] = useState(false);
  const [tipoSenhaError, setTipoSenhaError] = useState(false);
  const [tipoCpfError, setTipoCpfError] = useState(false);
  const [tipoSobrenomeError, setTipoSobrenomeError] = useState(false);
  const [tipoNomeError, setTipoNomeError] = useState(false);
  const [tipoSucesso, setTipoSucesso] = useState(false);
  const [validNome, setValidNome] = useState(false);
  const [validSobrenome, setValidSobrenome] = useState(false);
  const [validCpf, setValidCpf] = useState(false);
  const [validEmail, setValidEmail] = useState(false);
  const [validSenha, setValidSenha] = useState(false);
  const [autRequest, setAutRequest] = useState(true);


  useEffect(()=>{
    BuscarUserPorId(setJson);
},[]);

if(json.length > 0){
    setNome(json[0].info_user.nome);
    setValueNome(json[0].info_user.nome);
    setValidNome(true);
    
    setSobrenome(json[0].info_user.sobrenome);
    setValueSobrenome(json[0].info_user.sobrenome);
    setValidSobrenome(true);
    
    setCpf(cpfMask(json[0].info_user.cpf));
    setValueCpf(cpfMask(json[0].info_user.cpf));
    setValidCpf(true);

    setEmail(json[0].info_user.email);
    setValueEmail(json[0].info_user.email);
    setValidEmail(true);
    
    setJson([]);
}

  const verifySenhas = (reSenha) => {
    setReCadSenha(reSenha);
    if (reSenha === senha) {
      setMsgReSenha('');
      setValidSenha(true);
      habilitaRequest(true);
    } else {
      setMsgReSenha('As senhas não conferem');
      setValidSenha(false);
    }
  }

  const habilitaRequest = (pos) => {
    if ((pos || validSenha) && validNome && validSobrenome && validEmail && validCpf) {
      setAutRequest(false);
    } else {
      setAutRequest(true);
    }
  }

  const handleClick = () => {
    setAutRequest(true);
    setFecharTudo(true);
    Editar(nome, sobrenome, cpf, email, senha, setTipoSucesso, 
      setTipoCpfError, setTipoNomeError, setTipoSobrenomeError, 
      setTipoEmailError, setTipoSenhaError, setMsgEmail, setMsgCpf,
      setMsgSenha, setMsgSobrenome, setMsgNome);
  }


  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      if (!autRequest) {
        setFecharTudo(true);
        setAutRequest(true);
        Editar(nome, sobrenome, cpf, email, senha, setTipoSucesso, 
          setTipoCpfError, setTipoNomeError, setTipoSobrenomeError, 
          setTipoEmailError, setTipoSenhaError, setMsgEmail, setMsgCpf,
          setMsgSenha, setMsgSobrenome, setMsgNome)
      }
    }
  }

  const redireciona = () => {
    return (
      <Redirect to='/agendas' />
    )
  }

  return (
    <div className="container conteiner-edit">
      <div>
        <div className='img-conteiner-edit'>
          <img className='img-edit' src={avatarUser} alt='avatar criar usuario' />
          <h3 className='agenda-titulo-criar'>Editar Usuario</h3>
        </div>
        <div className="form-group">
          <input type="text"
            className={(!validNome && nome !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
            placeholder="Nome"
            value={valueNome}
            onFocus={() => {
              setTipoNomeError(false);
              setJson([]);
            }}
            onBlur={() => {
              habilitaRequest(false);
            }}
            onChange={input => {
              setValueNome(anulaEspacoMask(input.target.value));
              setNome(anulaEspacoMask(input.target.value));
              setValidNome(hasValidString(input.target.value));
              habilitaRequest(false);
            }}
          />
          {tipoNomeError && <span className='span-edit'> {msgNome} </span>}
        </div>
        <div className="form-group">
          <input type="text"
            className={(!validSobrenome && sobrenome !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
            placeholder="Sobrenome"
            value={valueSobrenome}
            onFocus={() => {
              setTipoSobrenomeError(false);
              setJson([]);
            }}
            onBlur={() => {
              habilitaRequest(false);
            }}
            onChange={input => {
              setValueSobrenome(anulaEspacoMask(input.target.value));
              setSobrenome(anulaEspacoMask(input.target.value));
              setValidSobrenome(hasValidString(input.target.value));
              habilitaRequest(false);
            }}
          />
          {tipoSobrenomeError && <span className='span-edit'> {msgSobrenome} </span>}
        </div>
        <div className="form-group">
          <input type="text"
            className={(!validCpf && cpf !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
            placeholder="Cpf"
            value={valueCpf}
            onFocus={() => {
              setTipoCpfError(false);
              setJson([]);
            }}
            onBlur={() => {
              habilitaRequest(false);
            }}
            onChange={input => {
              setValueCpf(cpfMask(input.target.value))
              setCpf(cpfMask(input.target.value));
              setValidCpf(hasValidCpf(input.target.value));
              habilitaRequest(false);
            }}
          />
          {tipoCpfError && <span className='span-edit'> {msgCpf} </span>}
        </div>
        <div className="form-group">
          <input type="email"
            className={(!validEmail && email !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
            placeholder="Email"
            value={valueEmail}
            onFocus={() => {
              setTipoEmailError(false);
              setJson([]);
            }}
            onBlur={() => {
              habilitaRequest(false);
            }}
            onChange={input => {
              setValueEmail(anulaEspacoMask(input.target.value));
              setEmail(anulaEspacoMask(input.target.value));
              setValidEmail(hasValidEmail(input.target.value));
              habilitaRequest(false);
            }}
          />
          {tipoEmailError && <span className='span-edit'> {msgEmail} </span>}
        </div>


        <div className="form-group">
          <input type="password"
            className="form-control input-edit"
            placeholder="Senha"
            value={valueSenha}
            onFocus={() => {
              setTipoSenhaError(false);
              setJson([]);
            }}
            onBlur={() => {
              habilitaRequest(false);
            }}
            onChange={input => {
              setValueSenha(anulaEspacoMask(input.target.value));
              setSenha(anulaEspacoMask(input.target.value))
              habilitaRequest(false);
            }}
            onKeyPress={event => handleKeyPress(event)}
          />
          {tipoSenhaError && <span className='span-edit'> {msgSenha} </span>}
        </div>

        <div className="form-group">
          <input type="password"
            className={(!validSenha && reCadSenha !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
            placeholder="Confirmação de Senha"
            value={valueReSenha}
            onFocus={() => {
              setTipoSenhaError(false);
              setJson([]);
            }}
            onBlur={input => {
              setValueReSenha(anulaEspacoMask(input.target.value));
              verifySenhas(input.target.value);
            }}
            onChange={input => {
              setValueReSenha(anulaEspacoMask(input.target.value));
              verifySenhas(input.target.value);
            }}
            onKeyPress={event => handleKeyPress(event)}
          />
          {tipoSenhaError && <span className='span-edit'> Senha inválida </span>}
          {(!validSenha && reCadSenha !== '') && <span className='span-edit'> {msgReSenha} </span>}
        </div>

        <div className='button-div-edit' >


          <button type="submit"
            disabled={autRequest ? 'disabled' : ''}
            className="btn btn-primary button-edit"
            onClick={() => handleClick()}
          >
            Cadastrar
                </button>
        </div>
        {tipoSucesso && redireciona()}
      </div>
    </div>
  );
}

export default EditarConta;