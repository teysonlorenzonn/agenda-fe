import React, { useState } from 'react';
import { Redirect } from 'react-router-dom'

import './css/login.css'
import { Autenticar } from './fetch/Login';
import { hasValidEmail } from '../../utils/Validacao';
import { anulaEspacoMask } from '../../utils/Mascaras';
import avatarUser from '../../img/icon/avatar-user.ico';

const Entrar = () => {
    const [valueEmail, setValueEmail] = useState('');
    const [valueSenha, setValueSenha] = useState('');

    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');
    
    const [token, setToken] = useState('');
    const [auth, setAuth] = useState(false);
    
    const [msgEmail, setMsgEmail] = useState('');
    const [msgSenha, setMsgSenha] = useState('');

    const [tipoEmailError, setTipoEmailError] = useState(false);
    const [tipoSenhaError, setTipoSenhaError] = useState(false);
    
    const [validEmail, setValidEmail] = useState(false);
    const [validSenha, setValidSenha] = useState(false);
    const [autRequest, setAutRequest] = useState(true);

    const habilitaRequest = () => {
        if (validEmail && validSenha) {
            setAutRequest(false);
        } else {
            setAutRequest(true);
        }
    }

    const handleClick = () => {
        setAutRequest(true);
        Autenticar(email, senha, setToken, setAuth, setTipoEmailError, setTipoSenhaError, setMsgEmail, setMsgSenha);
    }

    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            if (!autRequest) {
                setAutRequest(true);
                Autenticar(email, senha, setToken, setAuth, setTipoEmailError, setTipoSenhaError, setMsgEmail, setMsgSenha);
            }
        }
    }

    const redireciona = () => {
        if (token !== '' && token !== undefined) {
            return (
                <Redirect to='/' />
            )
        }
    }

    return (
        <div className="container conteiner-edit">
            <div >
                <div className='img-conteiner-edit'>
                    <img className='img-edit' src={avatarUser} alt='avatar usuario' />
                    <h3 className='agenda-titulo-criar'>Login</h3>
                </div>
                
                <div className="form-group">
                    <input type="email"
                        className={(!validEmail && email !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
                        placeholder="Email"
                        value={valueEmail}
                        onBlur={() => setValidSenha(true)}
                        onFocus={() => {
                            setTipoEmailError(false);
                            setAutRequest(false);
                            habilitaRequest();
                        }}
                        onChange={input => {
                            setValueEmail(anulaEspacoMask(input.target.value))
                            setEmail(input.target.value);
                            habilitaRequest();
                            setValidSenha(true);
                            setValidEmail(hasValidEmail(input.target.value));
                        }}
                    />
                    {tipoEmailError && <span className='span-edit'> {msgEmail} </span>}
                </div>

                <div className="form-group">
                    <input type="password"
                        className="form-control input-edit"
                        placeholder="Senha"
                        value={valueSenha}   
                        onBlur={() => setValidSenha(false)}   
                        onChange={input => {
                            setValueSenha(anulaEspacoMask(input.target.value));
                            setValidSenha(true);
                            setSenha(input.target.value);
                            habilitaRequest();
                        }}
                        onFocus={() => {
                            setTipoSenhaError(false);
                            setValidSenha(true);
                            habilitaRequest();
                        }}
                        onKeyPress={event => handleKeyPress(event)}
                    />
                    {tipoSenhaError && <span className='span-edit'> {msgSenha} </span>}
                </div>

                <div className='button-div-edit' >
                    <button type="submit"
                        disabled={autRequest ? 'disabled' : ''}
                        className="btn btn-primary button-edit"
                        onClick={() => handleClick()}
                    >
                        Entrar
                </button>
                </div>
                {auth && redireciona()}
            </div>
        </div>
    );
}

export default Entrar;