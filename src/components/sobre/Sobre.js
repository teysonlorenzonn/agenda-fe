import React from 'react';
import './css/sobre.css'
import Bit from '../../img/icon/bitbucket.ico'
const Sobre = () => {
    return(
        <div className="container conteiner-edit">
            <div className="jumbotron jumbotron-edit-ag">
                <h1 className="titulo-sobre">Desenvolvimento</h1>   
                <div className="desenvolv">
                    <p className="primeiro-p">Projeto foi desenvolvido no intuito de aplicar os conhecimentos adquiridos durante o 
                    oitavo semestre do curço de Ciencia da Computação na instituição URI Campus de Erechim em 2019. </p>
                    <p>As tecnologias utilizadas no projeto foram, frontend com React.js, backend com Node.js 
                    e banco de dados com MySql Server. o Projeto envolve também uma importante área da computação, os Sistemas Distribuidos, 
                    onde foi desenvolvido um código em Python para
                    concertar as possíveis falhas da comunicação entre o cliente e servidor, utilizando o algorítimo de 
                    bully, mais conhecido como o algorítimo do ditador, onde 3 ou mais servidores disponibilizam os dados do backend 
                    assim o algorítimo decide qual o servidor deve servir os dados no momento, caso o server caia por algum erro inesperado, 
                    assume se outro servidor assim evitando possíveis falhas na comunicação entre cliente e servidor.</p>
                    
                    <h4>Desenvolvedores:</h4>
                    <p className="primeiro-p">Felipe Wieczorek Passa - felipe_passa@hotmail.com</p>
                    <p>Teyson Lorenzon - teyson_lorenzon@hotmail.com</p>
                    
                    <h4>Repositórios:</h4>
                    <a className='link-sobre' href='https://bitbucket.org/teysonlorenzonn/agenda-fe/src/master/'>     
                        <p className="primeiro-p">
                            <img className='icones-bit' src={Bit} alt='icone bitbucket'/>
                            FrontEnd
                        </p>
                    </a>
                    <a className='link-sobre' href='https://bitbucket.org/teysonlorenzonn/agenda-api/src/master/' >
                        <p>
                            <img className='icones-bit' src={Bit} alt='icone bitbucket'/>
                            BackEnd
                        </p>
                    </a>
                </div>
                
                
            </div>
        </div>
        
    );
}

export default Sobre;