import React from 'react';
import './css/sobre.css'

const BemVindo = () => {
    return(
        <div className='div-bem-vindo'>
             <h1>Bem Vindo</h1>
             <p>Você está prestes a utilizar a melhor agenda de tarefas!</p>
        </div>
    );
}

export default BemVindo;