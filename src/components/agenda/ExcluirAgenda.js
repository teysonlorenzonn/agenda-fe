import React, { useState } from 'react';
import { Redirect } from 'react-router-dom'
import { Excluir } from './fetch/Agenda';
import { getIdAgenda, setIdAgenda } from '../../utils/Globals';

import './css/excluir-agenda.css';
import './css/agenda.css';

const ExcluirAgenda = () => {

    const [habRedir, setHabRedir] = useState(false);

    const handleExcluir = () => {
        Excluir(getIdAgenda());
        setIdAgenda(0);
        setHabRedir(true);
    }

    const redireciona = () => {
        return (<Redirect to='/agendas' />);
    }

    return (
        <div className='conteiner excluir-conteiner-edit-agenda'>
            <div className="jumbotron jumbotron-edit-ag">

                <div className="container internal-jumb-conteiner-edit">
                    <p className='p-edit-ag p-edit-excluir-ag'>Você deseja realmente remover ?</p>
                    <bottom
                        className="btn btn-primary btn-lg button-edit-excluir-ag"
                        onClick={() => {
                            handleExcluir();
                        }}
                    >Sim</bottom>
                    <bottom
                        className="btn btn-primary btn-lg button-edit-excluir-ag"
                        onClick={() => {
                            setHabRedir(true);
                        }}
                    >Nao</bottom>
                </div>

                {habRedir && redireciona()}
            </div>
        </div>
    )
}

export default ExcluirAgenda;