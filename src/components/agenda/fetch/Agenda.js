import { getToken, getIdUser } from '../../../utils/GerarCookies';
import { getIdAgenda, getLiberaFetch, setLiberaFetch } from '../../../utils/Globals';
import jso from '../../../server.json'

export const Cadastrar = (nome, tipo, setNomeError, setTipoError, setMsgNome, setMsgTipo, setSucesso) => {
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }

    const param = {
        'method': 'POST',
        'body': JSON.stringify({
            "nome": nome,
            "tipo": tipo,
            "id_user": getIdUser(),
        }),
        'headers': headers
    }

    fetch(`${jso.host}/agendas`, param)
        .then(response => response.json())
        .then(json => {
            if (!json.error) {
                setSucesso(true);
            }
            else {
                json.erros.forEach(erro => {
                    switch(erro) {
                        case 'error nome invalido':
                            setNomeError(true);
                            setMsgNome('Nome da Agenda não é valido');
                            break;
                        case 'error tipo invalido':
                            setTipoError(true);
                            setMsgTipo('Tipo não é valido');
                            break;
                        default:
                            break;
                    }
                })
                setSucesso(false);
            }

        })
        .catch(e => console.log('erro ao realizar cadastro de agenda:', e));
}
export const Editar = (nome, tipo, setNomeError, setTipoError, setMsgNome, setMsgTipo, setSucesso) => {
    if (getLiberaFetch) {
        const headers = {
            'Content-Type': 'application/json',
            'x-access-token': getToken()
        }

        const param = {
            'method': 'PUT',
            'body': JSON.stringify({
                "id": getIdAgenda(),
                "nome": nome,
                "tipo": tipo,
                "id_user": getIdUser(),
            }),
            'headers': headers
        }

        fetch(`${jso.host}/agendas`, param)
            .then(response => response.json())
            .then(json => {
                if (!json.error) {
                    setSucesso(true);
                }
                else {
                    json.erros.forEach(erro => {
                        switch(erro) {
                            case 'error nome invalido':
                                setNomeError(true);
                                setMsgNome('Nome da Agenda não é valido');
                                break;
                            case 'error tipo invalido':
                                setTipoError(true);
                                setMsgTipo('Tipo não é valido');
                                break;
                            default:
                                break;
                        }
                    })
                    setSucesso(false);
                }

            })
            .catch(e => console.log('erro ao realizar update da agenda:', e));
            setLiberaFetch(false);
    }
}

export const BuscarAgendaPorId = (setJson) => {
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }

    const param = {
        'method': 'GET',
        'headers': headers
    }

    fetch(`${jso.host}/agendas/${getIdAgenda()}`, param)
        .then(response => response.json())
        .then(json => {
            
            if (!json.error) {
                setJson(json);
            }

        })
        .catch(e => console.log('erro ao buscar agenda:', e));
}

export const BuscarAgendaPorTipo = (tipoAgenda,setAgendas) => {
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }

    const param = {
        'method': 'GET',
        'headers': headers
    }

    fetch(`${jso.host}/agendas/tipo/${tipoAgenda}`, param)
        .then(response => response.json())
        .then(json => {
            let jsonLogado = [];
            if (!json.error) {
                json.forEach(agenda => {
                    if (parseInt(agenda.info_user.id) === parseInt(getIdUser())) {
                        agenda.nome = agenda.nome.charAt(0).toUpperCase() + (agenda.nome.substr(1, agenda.nome.length).toLowerCase());
                        agenda.tipo = agenda.tipo.charAt(0).toUpperCase() + (agenda.tipo.substr(1, agenda.tipo.length).toLowerCase());
                        jsonLogado.push(agenda);
                    }
                });
            }
            setAgendas(jsonLogado);

        })
        .catch(e => console.log('erro ao buscar agenda:', e));
}

export const BuscarAgendas = (setAgendas) => {
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }

    const param = {
        'method': 'GET',
        'headers': headers
    }

    fetch(`${jso.host}/agendas`, param)
        .then(response => response.json())
        .then(json => {
            let jsonLogado = [];
            if (!json.error) {
                json.forEach(agenda => {
                    if (parseInt(agenda.info_user.id) === parseInt(getIdUser())) {
                        agenda.nome = agenda.nome.charAt(0).toUpperCase() + (agenda.nome.substr(1, agenda.nome.length).toLowerCase());
                        agenda.tipo = agenda.tipo.charAt(0).toUpperCase() + (agenda.tipo.substr(1, agenda.tipo.length).toLowerCase());
                        jsonLogado.push(agenda);
                    }
                });
            }
            setAgendas(jsonLogado);
        })
        .catch(e => console.log('erro ao buscar agendas:', e));
}

export const Excluir = (id) => {
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }

    const param = {
        'method': 'DELETE',
        'body': JSON.stringify({
            "id": id
        }),
        'headers': headers
    }

    fetch(`${jso.host}/agendas`, param)
        .then(response => response.json())
        .then(json => {
            if (!json.error) {
                console.log(json)
            }
            else {
                console.log(json)
            }

        })
        .catch(e => console.log('erro ao excluir agenda:', e));
}