import React, { useState } from 'react';
import { Redirect } from 'react-router-dom'

import './css/agenda.css';
import { Cadastrar } from './fetch/Agenda';
import { hasValidStringInteiros, hasValidString } from '../../utils/Validacao';
import avatarAgenda from '../../img/icon/agenda-cad.ico';
import { setFecharTudo } from '../../utils/Globals';

const CriarAgenda = () => {
    const [valueNome, setValueNome] = useState('');
    const [valueTipo, setValueTipo] = useState('');

    const [nome, setNome] = useState('');
    const [tipo, setTipo] = useState('');

    const [msgNome, setMsgNome] = useState('');
    const [msgTipo, setMsgTipo] = useState('');

    const [nomeError, setNomeError] = useState(false);
    const [tipoError, setTipoError] = useState(false);

    const [validNome, setValidNome] = useState(false);
    const [validTipo, setValidTipo] = useState(false);

    const [autRequest, setAutRequest] = useState(true);
    const [sucesso, setSucesso] = useState(false);



    const habilitaRequest = () => {
        if (validNome && validTipo) {
            setAutRequest(false);
        } else {
            setAutRequest(true);
        }
    }

    const handleClick = () => {
        setAutRequest(true);
        setFecharTudo(true);
        Cadastrar(nome, tipo, setNomeError, setTipoError, setMsgNome, setMsgTipo, setSucesso);
    }


    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            if (!autRequest) {
                setFecharTudo(true);
                setAutRequest(true);
                Cadastrar(nome, tipo, setNomeError, setTipoError, setMsgNome, setMsgTipo, setSucesso);
            }
        }
    }

    const redireciona = () => {
        return (
            <Redirect to='/agendas' />
        )
    }

    return (
        <div className="container conteiner-edit">
            <div>
                <div className='img-conteiner-edit'>
                    <img className='img-edit-agenda' src={avatarAgenda} alt='avatar criar usuario' />
                    <h3 className='agenda-titulo-criar'>Criar Agenda</h3>
                </div>

                <div className="form-group">
                    <input type="text"
                        className={(!validTipo && tipo !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
                        placeholder="Tipo"
                        value={valueTipo}
                        onFocus={() => setTipoError(false)}
                        onBlur={() => {
                            habilitaRequest();
                        }}
                        onChange={input => {
                            setValueTipo(input.target.value);
                            setTipo(input.target.value);
                            setValidTipo(hasValidString(input.target.value));
                            habilitaRequest();
                        }}
                        onKeyPress={event => handleKeyPress(event)}
                    />
                    {tipoError && <span className='span-edit'> {msgTipo} </span>}
                </div>

                <div className="form-group">
                    <input type="text"
                        className={(!validNome && nome !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
                        placeholder="Nome da Agenda"
                        value={valueNome}
                        onFocus={() => setNomeError(false)}
                        onBlur={() => {
                            habilitaRequest();
                        }}
                        onChange={input => {
                            setValueNome(input.target.value);
                            setNome(input.target.value);
                            setValidNome(hasValidStringInteiros(input.target.value));
                            habilitaRequest();
                        }}
                        onKeyPress={event => handleKeyPress(event)}
                    />
                    {nomeError && <span className='span-edit'> {msgNome} </span>}
                </div>
               

                <div className='button-div-edit' >


                    <button type="submit"
                        disabled={autRequest ? 'disabled' : ''}
                        className="btn btn-primary button-edit"
                        onClick={() => handleClick()}
                    >
                        Cadastrar
                </button>
                </div>
                {sucesso && redireciona()}
            </div>
        </div>
    );
}

export default CriarAgenda;