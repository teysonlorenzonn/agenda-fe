import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { setFecharTudo, setIdAgenda, setLiberaFetch, setActivePage, getActivePesquisaAgenda, setActivePesquisaAgenda} from '../../utils/Globals';

import { BuscarAgendas, BuscarAgendaPorTipo } from './fetch/Agenda';
import './css/agenda.css';

import remove from '../../img/icon/remove-agendas.ico';
import edit from '../../img/icon/edit-agendas.ico';
import exit from '../../img/icon/sair.ico';

const ListarAgendas = () => {
    const [agendas, setAgendas] = useState([]);
    const [tipoAgenda,setTipoAgenda] = useState('');

    useEffect(() => {
        BuscarAgendas(setAgendas);
    }, []);

    const filtrarAgendas = (event) => {

        if(event.keyCode === 27){
            BuscarAgendas(setAgendas);
            setActivePesquisaAgenda(false);
            setFecharTudo(true);
        }

        if (event.key === 'Enter') {
            BuscarAgendaPorTipo(tipoAgenda,setAgendas);
        }
    }

    return (
        <div className='container container-edit-la'>
                <h1 className='agenda-titulo'>Agendas</h1>
                {getActivePesquisaAgenda() && <div className='div-input-pesquisar'>
                <div>
                <input 
                type='text' 
                className='form-control input-edit-pesquisar'
                placeholder='Pesquisar por tipo'
                onChange={input => setTipoAgenda(input.target.value)}
                onKeyUp={event => filtrarAgendas(event)}
                />
                <Link to='/agendas' onClick={() => {
                    BuscarAgendas(setAgendas);
                    setActivePesquisaAgenda(false);
                    setFecharTudo(true);
                }}>
                    <img className='exit-pesquisar' src={exit} alt='cancelar pesquisa'/>
                </Link>
                </div> 
            </div>}
            <div className="row">
                {agendas && agendas.map(agenda => {
                    return (
                        <div className="col-md-6 div-jumb-edit" key={agenda.id}>
                            <div className="jumbotron jumbotron-edit-ag">
                                <div className='pos-icon-ag'>
                                    <Link
                                        to='/agenda/editar'
                                        onClick={() => {
                                            setIdAgenda(agenda.id);
                                            setLiberaFetch(true);
                                        }}
                                    >
                                        <img className='edit-icon-ag' src={edit} alt='editar' /></Link>
                                    <Link
                                        to='/agenda/excluir'
                                        onClick={() => {
                                            setIdAgenda(agenda.id);
                                        }}
                                    >
                                        <img className='remove-icon-ag' src={remove} alt='excluir' />
                                    </Link>
                                </div>
                                <div className="container internal-jumb-conteiner-edit">
                                        <h1 className='h1-edit-ag'>{agenda.tipo}</h1>
                                        <p className='p-edit-ag'>{agenda.nome}</p>
                                    <Link
                                    to='/agenda/paginas'
                                    className="btn btn-primary btn-lg bottom-edit-ag"
                                    onClick={() => {
                                        setIdAgenda(agenda.id);
                                        setActivePage(true);
                                    }} 
                                    >Entrar</Link>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export default ListarAgendas;