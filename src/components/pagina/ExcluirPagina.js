import React, { useState } from 'react';
import { Redirect } from 'react-router-dom'
import { Excluir } from './fetch/Pagina';
import { getIdPagina, setIdPagina } from '../../utils/Globals';


const ExcluirPagina = () => {

    const [habRedir, setHabRedir] = useState(false);

    const handleExcluir = () => {
        Excluir(getIdPagina());
        setIdPagina(0);
        setHabRedir(true);
    }

    const redireciona = () => {
        return (<Redirect to='/agenda/paginas' />);
    }

    return (
        <div className='conteiner excluir-conteiner-edit-agenda'>
            <div className="jumbotron jumbotron-edit-ag">

                <div className="container internal-jumb-conteiner-edit">
                    <p className='p-edit-ag p-edit-excluir-ag'>Você deseja realmente remover ?</p>
                    <bottom
                        className="btn btn-primary btn-lg button-edit-excluir-ag"
                        onClick={() => {
                            handleExcluir();
                        }}
                    >Sim</bottom>
                    <bottom
                        className="btn btn-primary btn-lg button-edit-excluir-ag"
                        onClick={() => {
                            setHabRedir(true);
                        }}
                    >Nao</bottom>
                </div>

                {habRedir && redireciona()}
            </div>
        </div>
    )
}

export default ExcluirPagina;