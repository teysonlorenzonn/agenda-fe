import React, { useState } from 'react';
import { Redirect } from 'react-router-dom'
import { Cadastrar } from './fetch/Pagina';
import { dataBr, horaBr } from '../../utils/Mascaras';
import { hasValidStringInteiros, hasValidDataBr, hasValidHoraBr, normalizeDataParaCadastro } from '../../utils/Validacao';
import avatarPagina from '../../img/icon/criar-paginas.ico';
import { setFecharTudo } from '../../utils/Globals';

const CriarPagina = () => {
    const [valueNome, setValueNome] = useState('');
    const [valueDescricao, setValueDescricao] = useState('');
    const [valueDataInicial, setValueDataInicial] = useState('');
    const [valueDataFinal, setValueDataFinal] = useState('');
    const [valueHoraInicial, setValueHoraInicial] = useState('');
    const [valueHoraFinal, setValueHoraFinal] = useState('');

    const [nome, setNome] = useState('');
    const [descricao, setDescricao] = useState('');
    const [dataInicial, setDataInicial] = useState('');
    const [dataFinal, setDataFinal] = useState('');
    const [horaInicial, setHoraInicial] = useState('');
    const [horaFinal, setHoraFinal] = useState('');

    const [msgNome, setMsgNome] = useState('');
    const [msgDescricao, setMsgDescricao] = useState('');
    const [msgDataInicial, setMsgDataInicial] = useState('');
    const [msgDataFinal, setMsgDataFinal] = useState('');
    const [msgHoraInicial, setMsgHoraInicial] = useState('');
    const [msgHoraFinal, setMsgHoraFinal] = useState('');

    const [nomeError, setNomeError] = useState(false);
    const [descricaoError, setDescricaoError] = useState(false);
    const [dataInicialError, setDataInicialError] = useState(false);
    const [dataFinalError, setDataFinalError] = useState(false);
    const [horaInicialError, setHoraInicialError] = useState(false);
    const [horaFinalError, setHoraFinalError] = useState(false);

    const [validNome, setValidNome] = useState(false);
    const [validDescricao, setValidDescricao] = useState(false);
    const [validDataInicial, setValidDataInicial] = useState(false);
    const [validDataFinal, setValidDataFinal] = useState(false);
    const [validHoraInicial, setValidHoraInicial] = useState(false);
    const [validHoraFinal, setValidHoraFinal] = useState(false);

    const [dataIniConv,setDataIniConv] = useState('');
    const [dataFiniConv,setDatafiniConv] = useState('');

    const [autRequest, setAutRequest] = useState(true);
    const [sucesso, setSucesso] = useState(false);

    const [msgSpanCentral, setMsgSpanCentral] = useState('')
    const [spanCentralError, setSpanCentralError] = useState(false);


    const habilitaRequest = () => {
        if (validNome && validDescricao && validDataInicial && validDataFinal
            && validHoraInicial && validHoraFinal) {
            setAutRequest(false);
            setDataIniConv(normalizeDataParaCadastro(dataInicial, horaInicial));
            setDatafiniConv(normalizeDataParaCadastro(dataFinal, horaFinal));  
        } else {
            setAutRequest(true);
        }
    }

    const handleClick = () => {
        setAutRequest(true);
        setFecharTudo(true);
        Cadastrar(nome, descricao, dataIniConv, dataFiniConv, setNomeError,
            setDescricaoError, setDataInicialError, setDataFinalError, 
            setHoraInicialError, setHoraFinalError,setSucesso,
            setMsgNome, setMsgDescricao, setMsgDataInicial, setMsgDataFinal, 
            setMsgHoraInicial, setMsgHoraFinal, setSpanCentralError, setMsgSpanCentral);
    }


    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            if (!autRequest) {
                setFecharTudo(true);
                setAutRequest(true);
                Cadastrar(nome, descricao, dataIniConv, dataFiniConv, setNomeError,
                    setDescricaoError, setDataInicialError, setDataFinalError, 
                    setHoraInicialError, setHoraFinalError,setSucesso,
                    setMsgNome, setMsgDescricao, setMsgDataInicial, setMsgDataFinal, 
                    setMsgHoraInicial, setMsgHoraFinal);
            }
        }
    }

    const redireciona = () => {
        return (
            <Redirect to='/agenda/paginas' />
        )
    }

    return (
        <div className="container conteiner-edit">
            <div>
                <div className='img-conteiner-edit'>
                    <img className='img-edit-agenda' src={avatarPagina} alt='avatar criar usuario' />
                    <h3 className='agenda-titulo-criar'>Criar Compromisso</h3>
                </div>
                <div className='div-span-central-edit'>
                    {spanCentralError && <span className='span-edit'> {msgSpanCentral} </span> }
                </div>
                <div className="form-group">

                    <input type="text"
                        className={(!validNome && nome !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
                        placeholder="Nome"
                        value={valueNome}
                        onFocus={() => setNomeError(false)}
                        onBlur={() => {
                            habilitaRequest();
                        }}
                        onChange={input => {
                            setValueNome(input.target.value);
                            setNome(input.target.value);
                            setValidNome(hasValidStringInteiros(input.target.value));
                            habilitaRequest();
                        }}
                        onKeyPress={event => handleKeyPress(event)}
                    />
                    {nomeError && <span className='span-edit'> {msgNome} </span>}
                </div>

                <div className="form-group">
                    <input type="text"
                    className={(!validDataInicial && dataInicial !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
                    placeholder="Data inicial"
                    value={valueDataInicial}
                    onFocus={() => setDataInicialError(false)}
                    onBlur={()=> {
                        habilitaRequest();
                    }}
                    onChange={input => {
                        setValueDataInicial(dataBr(input.target.value));
                        setDataInicial(dataBr(input.target.value));
                        setValidDataInicial(hasValidDataBr(input.target.value));
                        habilitaRequest();
                    }}
                    onKeyPress={event => handleKeyPress(event)}
                    />
                    {dataInicialError && <span className='span-edit'> {msgDataInicial} </span>}
                </div>

                <div className="form-group">
                    <input type="text"
                    className={(!validDataFinal && dataFinal !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
                    placeholder="Data final"
                    value={valueDataFinal}
                    onFocus={() => setDataFinalError(false)}
                    onBlur={() => {
                        habilitaRequest();
                    }}
                    onChange={input => {
                        setValueDataFinal(dataBr(input.target.value));
                        setDataFinal(dataBr(input.target.value));
                        setValidDataFinal(hasValidDataBr(input.target.value));
                        habilitaRequest();
                    }}
                    onKeyPress={event => handleKeyPress(event)}
                    />
                    {dataFinalError && <span className='span-edit'> {msgDataFinal} </span>}
                </div>
                <div className="form-group">
                <input type="text"
                className={(!validHoraInicial && horaInicial !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
                placeholder="Hora inicial"
                value={valueHoraInicial}
                onFocus={() => setHoraInicialError(false)}
                onBlur={input => {
                    habilitaRequest();
                }}
                onChange={input => {
                    setValueHoraInicial(horaBr(input.target.value));
                    setHoraInicial(horaBr(input.target.value));
                    setValidHoraInicial(hasValidHoraBr(input.target.value));
                    habilitaRequest();
                }}
                onKeyPress={event => handleKeyPress(event)}
                />
                {horaInicialError && <span className='span-edit'> {msgHoraInicial} </span>}
            </div>

            <div className="form-group">
                <input type="text"
                className={(!validHoraFinal && horaFinal !== '') ? 'form-control input-edit input-edit-error' : 'form-control input-edit'}
                placeholder="Hora final"
                value={valueHoraFinal}
                onFocus={() => setHoraFinalError(false)}
                onBlur={() => {
                    habilitaRequest();
                }}
                onChange={input => {
                    setValueHoraFinal(horaBr(input.target.value));
                    setHoraFinal(horaBr(input.target.value));
                    setValidHoraFinal(hasValidHoraBr(input.target.value));
                    habilitaRequest();
                }}
                onKeyPress={event => handleKeyPress(event)}
                />
                {horaFinalError && <span className='span-edit'> {msgHoraFinal} </span>}
            </div>

            <div className="form-group">
                <textarea
                    type="text"
                    className={(!validDescricao && descricao !== '') ? 'form-control input-edit input-edit-error desc-edit-pg' : 'form-control input-edit desc-edit-pg'}
                    placeholder="Descrição"
                    value={valueDescricao}
                    onFocus={() => setDescricaoError(false)}
                    onBlur={() => {
                        habilitaRequest();
                    }}
                    onChange={input => {
                        setValueDescricao(input.target.value);
                        setDescricao(input.target.value);
                        setValidDescricao(true);
                        habilitaRequest();
                    }}
                    onKeyPress={event => handleKeyPress(event)}
                />
                {descricaoError && <span className='span-edit'> {msgDescricao} </span>}
            </div>
            <div className='button-div-edit' >
                <button type="submit"
                    disabled={autRequest ? 'disabled' : ''}
                    className="btn btn-primary button-edit"
                    onClick={() => handleClick()}
                >
                    Cadastrar
                </button>
            </div>
            {sucesso && redireciona()}
        </div>
    </div>
    );
}

export default CriarPagina;