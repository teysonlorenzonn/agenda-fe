import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { BuscarPaginas, BuscarPaginaPorNome } from './fetch/Pagina';
import { setFecharTudo, setIdPagina, setLiberaFetch, setIdAgenda, getActivePesquisaPage, setActivePesquisaPage } from '../../utils/Globals';

import './css/pagina.css';
import remove from '../../img/icon/remove-agendas.ico'
import edit from '../../img/icon/edit-agendas.ico'
import exit from '../../img/icon/sair.ico';

const ListarPaginas = () => {
    const [paginas, setPaginas] = useState([]);
    const [nomePagina, setNomePagina] = useState('');

    useEffect(() => {
        BuscarPaginas(setPaginas);
    }, []);
    
    const filtrarPaginas = (event) => {
        if(event.keyCode === 27){
            BuscarPaginas(setPaginas);
            setActivePesquisaPage(false);
            setFecharTudo(true);
        }

        if (event.key === 'Enter') {
            BuscarPaginaPorNome(nomePagina,setPaginas);
        }
    }

    return (
        <div className='container container-edit-la'>
        <h1 className='agenda-titulo'>Meus Compromissos</h1>
        {getActivePesquisaPage() && <div className='div-input-pesquisar'>
                <div>
                <input 
                type='text' 
                className='form-control input-edit-pesquisar'
                placeholder='Pesquisar por nome'
                onChange={input => setNomePagina(input.target.value)}
                onKeyUp={event => filtrarPaginas(event)}
                />
                <Link to='/agenda/paginas' onClick={() => {
                    BuscarPaginas(setPaginas);
                    setActivePesquisaPage(false);
                    setFecharTudo(true);
                }}>
                    <img className='exit-pesquisar' src={exit} alt='cancelar pesquisa'/>
                </Link>
                </div> 
            </div>}
            <div className="row">
                {paginas && paginas.map(pagina => {
                    return (
                        <div className="col-md-6 div-jumb-edit" key={pagina.id}>
                            <div className="jumbotron jumbotron-edit-ag">
                                <div className='pos-icon-ag'>
                                    <Link
                                        to='/agenda/pagina/editar'
                                        onClick={() => {
                                            setIdPagina(pagina.id);
                                            setIdAgenda(pagina.info_agenda.id);
                                            setLiberaFetch(true);
                                        }}
                                    >
                                        <img className='edit-icon-ag' src={edit} alt='editar' /></Link>
                                    <Link
                                        to='/agenda/pagina/excluir'
                                        onClick={() => {
                                            setIdPagina(pagina.id);
                                        }}
                                    >
                                        <img className='remove-icon-ag' src={remove} alt='excluir' />
                                    </Link>
                                </div>
                                <div className="container">
                                    <h1 className='h1-edit-ag'>{pagina.nome}</h1>
                                    <p className='p-edit-pg'>{pagina.descricao}</p>


                                    <table className="table-edit-pg">
                                        <thead>
                                            <tr>
                                                <th scope="col">Data Inicial</th>
                                                <th scope="col">Data Final</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td className='td-edit-esq-pg'>{pagina.data_inicial}</td>
                                                <td className='td-edit-dir-pg'>{pagina.data_final}</td>
                                            </tr>
                                            <tr>
                                                <td className='td-edit-esq-pg'>{pagina.hora_inicial}</td>
                                                <td className='td-edit-dir-pg'>{pagina.hora_final}</td>
                                            </tr>
                                        </tbody>
                                    </table>



                                </div>
                            </div>
                        </div>

                    );
                })}
            </div>
        </div>
    );
}

export default ListarPaginas;

