import { getToken, getIdUser } from '../../../utils/GerarCookies';
import { getIdPagina, getLiberaFetch, setLiberaFetch, getIdAgenda } from '../../../utils/Globals';
import { converteData } from '../../../utils/Validacao';
import jso from '../../../server.json'


export const Cadastrar = (nome, descricao, dataInicial, dataFinal, setNomeError,
    setDescricaoError, setDataInicialError, setDataFinalError, 
    setHoraInicialError, setHoraFinalError,setSucesso,
    setMsgNome, setMsgDescricao, setMsgDataInicial, setMsgDataFinal, 
    setMsgHoraInicial, setMsgHoraFinal) => { 
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }

    const param = {
        'method': 'POST',
        'body': JSON.stringify({
            "nome": nome,
            "descricao": descricao,
            "data_inicio": dataInicial,
            "data_final": dataFinal,
            "id_agenda": getIdAgenda(),
        }),
        'headers': headers
    }

    fetch(`${jso.host}/paginas`, param)
        .then(response => response.json())
        .then(json => {
            if (!json.error && !json.erros) {
                setSucesso(true);
            }
            else {
                json.erros.forEach(erro => {
                    switch(erro) {
                        case 'erro nome ivalido':
                            setNomeError(true);
                            setMsgNome('Nome da Agenda não é valido');
                            break;
                        case 'erro descricao invalida':
                            setDescricaoError(true);
                            setMsgDescricao('Descrição não é valida');
                            break;
                        case 'erro data inicio invalida':
                            setDataInicialError(true);
                            setHoraInicialError(true);
                            setMsgDataInicial('Data inicial não é valida');
                            setMsgHoraInicial('Hora inicial não é valida');
                            break;
                        case 'erro data final invalida':
                            setDataFinalError(true);
                            setHoraFinalError(true);
                            setMsgHoraFinal('Hora final não é valida');
                            setMsgDataFinal('Data final não é valida');
                            break;
                        case 'erro hora final e menor do que a hora inicial':
                            setHoraFinalError(true);
                            setMsgHoraFinal('Hora final deve ser maior do que a inicial');
                            break;
                        case 'erro data final e menor do que a data inicial':
                            setMsgDataFinal('Data final deve ser maior do que a inicial');
                            setDataFinalError(true);
                            break;
                        default:
                            break;
                    }

                })
                setSucesso(false);
            }

        })
        .catch(e => console.log('erro ao realizar cadastro de pagina:', e));
}    


export const Editar = (nome, descricao, dataInicial, dataFinal, setNomeError,
    setDescricaoError, setDataInicialError, setDataFinalError, 
    setHoraInicialError, setHoraFinalError,setSucesso,
    setMsgNome, setMsgDescricao, setMsgDataInicial, setMsgDataFinal, 
    setMsgHoraInicial, setMsgHoraFinal) => {
    if (getLiberaFetch) {
        const headers = {
            'Content-Type': 'application/json',
            'x-access-token': getToken()
        }
        console.log(getIdAgenda())
        const param = {
            'method': 'PUT',
            'body': JSON.stringify({
                "id": getIdPagina(),
                "nome": nome,
                "descricao": descricao,
                "data_inicio": dataInicial,
                "data_final": dataFinal,
                "id_agenda": getIdAgenda()
            }),
            'headers': headers
        }

        fetch(`${jso.host}/paginas`, param)
        .then(response => response.json())
        .then(json => {
            if (!json.error && !json.erros) {
                setSucesso(true);
            }
            else {
                json.erros.forEach(erro => {
                    switch(erro) {
                        case 'erro nome ivalido':
                            setNomeError(true);
                            setMsgNome('Nome da Agenda não é valido');
                            break;
                        case 'erro descricao invalida':
                            setDescricaoError(true);
                            setMsgDescricao('Descrição não é valida');
                            break;
                        case 'erro data inicio invalida':
                            setDataInicialError(true);
                            setHoraInicialError(true);
                            setMsgDataInicial('Data inicial não é valida');
                            setMsgHoraInicial('Hora inicial não é valida');
                            break;
                        case 'erro data final invalida':
                            setDataFinalError(true);
                            setHoraFinalError(true);
                            setMsgHoraFinal('Hora final não é valida');
                            setMsgDataFinal('Data final não é valida');
                            break;
                        case 'erro hora final e menor do que a hora inicial':
                            setHoraFinalError(true);
                            setMsgHoraFinal('Hora final deve ser maior do que a inicial');
                            break;
                        case 'erro data final e menor do que a data inicial':
                            setMsgDataFinal('Data final deve ser maior do que a inicial');
                            setDataFinalError(true);
                            break;
                        default:
                            break;
                    }

                })
                setSucesso(false);
            }

        })
        .catch(e => console.log('erro ao realizar edicao de pagina:', e));
            setLiberaFetch(false);
    }
}

export const BuscarPaginaPorId = (setJson) => {
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }

    const param = {
        'method': 'GET',
        'headers': headers
    }

    fetch(`${jso.host}/paginas/${getIdPagina()}`, param)
        .then(response => response.json())
        .then(json => {
            
            if (!json.error) {
                let dataI = converteData(json[0].data_inicial);
                let dataF = converteData(json[0].data_final);
                json[0].data_inicial = dataI[0];
                json[0].data_final = dataF[0];
                json[0]['hora_inicial'] = dataI[1];
                json[0]['hora_final'] = dataF[1];
                setJson(json);
            }

        })
        .catch(e => console.log('erro ao buscar agenda:', e));
}


export const BuscarPaginaPorNome = (nomePagina,setPaginas) => {
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }

    const param = {
        'method': 'GET',
        'headers': headers
    }

    fetch(`${jso.host}/paginas/nome/${nomePagina}`, param)
        .then(response => response.json())
        .then(json => {
            let jsonLogado = [];
            if (!json.error) {
                json.forEach(pagina => {
                    if (parseInt(pagina.info_agenda.id) === parseInt(getIdAgenda())) {
                        if(parseInt(pagina.info_agenda.info_user.id)  === parseInt(getIdUser())){
                            let dataIni = converteData(pagina.data_inicial);
                            let dataFina = converteData(pagina.data_final);
                            
                            pagina.nome = pagina.nome.charAt(0).toUpperCase() + (pagina.nome.substr(1, pagina.nome.length).toLowerCase());
                            pagina.descricao = pagina.descricao.charAt(0).toUpperCase() + (pagina.descricao.substr(1, pagina.descricao.length).toLowerCase());
                            
                            pagina.data_inicial = dataIni[0];
                            pagina.data_final = dataFina[0];
                            pagina['hora_inicial'] = dataIni[1];
                            pagina['hora_final'] = dataFina[1];
                            jsonLogado.push(pagina);
                        }
                    }
                });
            }
            setPaginas(jsonLogado);

        })
        .catch(e => console.log('erro ao buscar agenda:', e));
}


export const BuscarPaginas = (setPaginas) => {
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }

    const param = {
        'method': 'GET',
        'headers': headers
    }

    fetch(`${jso.host}/paginas`, param)
        .then(response => response.json())
        .then(json => {
            let jsonLogado = [];
            if (!json.error) {
                json.forEach(pagina => {
                    if (parseInt(pagina.info_agenda.id) === parseInt(getIdAgenda())) {
                        if(parseInt(pagina.info_agenda.info_user.id)  === parseInt(getIdUser())){
                            let dataIni = converteData(pagina.data_inicial);
                            let dataFina = converteData(pagina.data_final);
                            
                            pagina.nome = pagina.nome.charAt(0).toUpperCase() + (pagina.nome.substr(1, pagina.nome.length).toLowerCase());
                            pagina.descricao = pagina.descricao.charAt(0).toUpperCase() + (pagina.descricao.substr(1, pagina.descricao.length).toLowerCase());
                            
                            pagina.data_inicial = dataIni[0];
                            pagina.data_final = dataFina[0];
                            pagina['hora_inicial'] = dataIni[1];
                            pagina['hora_final'] = dataFina[1];
                            jsonLogado.push(pagina);
                        }
                    }
                });
            }
            setPaginas(jsonLogado);
        })
        .catch(e => console.log('erro ao buscar agendas:', e));
}


export const Excluir = (id) => {
    const headers = {
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }

    const param = {
        'method': 'DELETE',
        'body': JSON.stringify({
            "id": id
        }),
        'headers': headers
    }

    fetch(`${jso.host}/paginas`, param)
        .then(response => response.json())
        .then(json => {
            if (!json.error) {
                console.log(json)
            }
            else {
                console.log(json)
            }

        })
        .catch(e => console.log('erro ao excluir pagina:', e));
}