const regString = /^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/;
const regInteiros = /[0-9]+$/;
const regEmail = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
const regStringInteiros = /^[çA-Za-z0-9 ]+$/
const regData = /^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|11)(-)(0[1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|[12][0-9]|2[0-8]))|(([02468][048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(02)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02)(-)(29)))(\s)(([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))$/;
const regDataBr = /^((0[1-9])|([12][0-9])|(3[0-1]))\/((0[1-9])|(1[0-2]))\/([1234][0-9]{3})/;
const regHora = /^(([01][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))/

export const hasValidEmail = email => regEmail.test(email);
export const hasValidString = stri => regString.test(stri);
export const hasValidInteiro = inti => regInteiros.test(inti);
export const hasValidStringInteiros = value => regStringInteiros.test(value);
export const hasValidDate = value => regData.test(value);
export const hasValidDataBr = value => regDataBr.test(value);
export const hasValidHoraBr = value => regHora.test(value);

export const hasValidCpf = cpf => {
    
    let soma;
    let resto;

    let strCPF = normalizeCpf(cpf);

    if(!hasValidInteiro(strCPF)) return false;

    soma = 0;
    if (strCPF === "00000000000") return false;

    for (let i = 1; i <= 9; i++) soma = soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    resto = (soma * 10) % 11;

    if ((resto === 10) || (resto === 11)) resto = 0;
    if (resto !== parseInt(strCPF.substring(9, 10))) return false;

    soma = 0;
    for (let i = 1; i <= 10; i++) soma = soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    resto = (soma * 10) % 11;

    if ((resto === 10) || (resto === 11)) resto = 0;
    if (resto !== parseInt(strCPF.substring(10, 11))) return false;
    return true;
};

export const normalizeCpf = cpf => {
    let strCPF = '';

    for(let i = 0; i < cpf.length; i++) {
        if (regInteiros.test(cpf.charAt(i))) {
            strCPF += cpf.charAt(i);
        }
    }

    return strCPF;
}

export const converteData = (data) => {
    
    let dataI = data.split('T');
    let hora = dataI[1].replace('.000Z','');
    dataI = dataI[0].split('-');
    
    let dia = dataI[2];
    let mes = dataI[1];
    let ano = dataI[0];

    return  [`${dia}/${mes}/${ano}`, `${hora}`]
}

export const normalizeDataParaCadastro = (data, hora) => {
    let dataI = data.split('/');
    
    let dia = dataI[0];
    let mes = dataI[1];
    let ano = dataI[2];

    return `${ano}-${mes}-${dia} ${hora}`;
}