let liberaFetch = false;
let idAgenda = 0;
let idPagina = 0;
let activePage = false;
let activePesquisaPage = false;
let activePesquisaAgenda = false;
let fecharTudo = false;
let fechaPesquisaTrocaPage = true;

export const setFechaPesquisaTrocaPage = active => fechaPesquisaTrocaPage=active;
export const getFechaPesquisaTrocaPage = () => {
    return fechaPesquisaTrocaPage;
}

export const setFecharTudo = active => fecharTudo=active;
export const getFecharTudo = () => {
    return fecharTudo;
}

export const setActivePesquisaPage = active => activePesquisaPage=active;
export const getActivePesquisaPage = () => {
    return activePesquisaPage;
}

export const setActivePesquisaAgenda = active => activePesquisaAgenda=active;
export const getActivePesquisaAgenda = () => {
    return activePesquisaAgenda;
}

export const setActivePage = active => activePage=active;
export const getActivePage = () => {
    return activePage;
}

export const setIdAgenda = id => idAgenda=id;
export const getIdAgenda = () => {
    return idAgenda;
}

export const setIdPagina = id => idPagina=id;
export const getIdPagina = () => {
    return idPagina;
}

export const setLiberaFetch = bool => liberaFetch=bool;
export const getLiberaFetch = () => {
    return liberaFetch;
}