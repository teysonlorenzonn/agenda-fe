
export const getCookie = () => {
    let tmp = document.cookie.split('; ');
    let cookie = {};
    tmp.forEach(it => {
        let corta = it.split('=');
        cookie[corta[0]] = corta[1];
    })
    return cookie;
}

export const getAuth = () => {
    let cookie = getCookie();

    if (cookie.autenticado) {
        if (cookie.autenticado === 'false') return false;
        else if (cookie.autenticado === 'true') return true;
    }

    return false;
}

export const getToken = () => {
    let cookie = getCookie();

    if (cookie.autenticado) {
        return cookie.tokenSession;
    }

    return '';
}

export const getIdUser = () => {
    let cookie = getCookie();

    if (cookie.autenticado) {
        return cookie.sessionId;
    }

    return '';
}

export const setCookie = (name, value, exdays) => {
    let expires;
    let date = new Date();

    date.setTime(date.getTime() + (exdays * 24 * 60 * 60 * 1000));
    expires = date.toUTCString();

    document.cookie = `${name}=${value}; expires=${expires}; path=/`;
}

