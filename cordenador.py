import socket
import time
import json

def test_port(ip, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((ip, port))
    sock.close()
    if result == 0:
        return True
    else:
        return False

def quem_comanda(maquinas, porta, array_id):
    count = len(maquinas)
    array_anterior = []
    for id in array_id:
        array_anterior.append(id)
    while True:
        maior_id = 0
        if(count == 0 or len(array_anterior) <= 0):
            break
        for id in array_anterior:
            if (id > maior_id):
                maior_id = id
        teste = test_port(maquinas[maior_id], porta)
        if (not teste):
            if(maior_id in array_anterior):
                array_anterior.remove(maior_id)
        else:
            return maior_id
        count -= 1

    return -1


maquinas = {}
array_id = []
porta = int(input('Porta dos Servidores: '))
diretorio = str(input('Caminho do Diretório: '))

quant = int(input('Numero de maquinas: '))

for i in range(0,quant):
    print('Maquina {0}:'.format((i+1)))
    ip = str(input('Ip: '))
    id = int(input('Id: '))
    array_id.append(id)
    maquinas[id] = ip

print()
atual = '';
while True:
    id = quem_comanda(maquinas, porta, array_id)
    if (id == -1):
        break
    if(atual != maquinas[id]):
        atual = maquinas[id]
        arquivo = open(diretorio, 'w')
        hosts = 'http://{0}:{1}'.format(atual, porta)
        jso = {
            "host": hosts
        }
        arquivo.write(json.dumps(jso))
        arquivo.close()
        print('Trocou para {0}'.format(hosts))
        print('Server ativo ip:{0} porta:{1}'.format(maquinas[id], porta))

    time.sleep(3)

arquivo = open(diretorio , 'w')
atual = 'agenda-rest-api.herokuapp.com'
hosts = 'https://{0}'.format(atual)
jso = {
    "host": hosts
}
arquivo.write(json.dumps(jso))
arquivo.close()
print()
print('Todos os servidores desligados')
print("Voltou para host remoto: {0}".format(hosts))